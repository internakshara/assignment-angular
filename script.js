var app = angular.module('filterApplication',[])

app.controller('filterController',['$scope', function ($scope) {
	$scope.type = 'firstName';
	$scope.names = [
	{
		firstName: "Anitha" ,
		lastName: "Kuppusamy"
	},
	{
		firstName: "Anitha" ,
		lastName: "Sundharam"
	},
	{
		firstName: "Usha" ,
		lastName: "Kanagaraj"
	},
	{
		firstName: "Anitha" ,
		lastName: "Sanmugam"
	},
	{
		firstName: "Kanagaraj" ,
		lastName: "Ponnusamy"
	}
	];
}]);